#!/usr/bin/php

<?php
require 'database.php';
$db = null;
try {
    echo 'Start configuring project ...' .PHP_EOL;
    echo 'Setup Database ...' .PHP_EOL;
    $db = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD, $DB_OPTIONS);
    $sql = file_get_contents('template.sql');
    $db->exec($sql);
    echo 'Project successfully setup';
}
catch (PDOException $ex) {
    echo $ex;
    die();
}
finally {
    $db = null;
}