<?php

$DB_DSN = 'mysql:dbname=camagru;host=127.0.0.1';
$DB_USER = 'root';
$DB_PASSWORD = 'qwerty';
$DB_OPTIONS = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
];