<?php
return [

    ['GET', '/home', 'Account#index'],
    ['GET', '/accounts', 'Account#index'],
    ['GET', '/accounts/:id', 'Account#show'],

    ['GET', '/delire/:id', 'Account#delete']
];
