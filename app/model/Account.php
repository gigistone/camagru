<?php

namespace app\model;

use \core\db\Table;
use \core\db\Database;

class Account extends Table
{
    static function findByUsername($name) {
        $query = 'SELECT * FROM account WHERE username = ? LIMIT 1';
        $db = Database::getCursor();
        return $db->query($query, \get_called_class(), [$name], true);
    }

    static function findByEmail($email) {
        $query = 'SELECT * FROM account WHERE mail = ? LIMIT 1';
        $db = Database::getCursor();
        return $db->query($query, \get_called_class(), [$email], true);
    }
}
