<?php

namespace app\model;

use \core\db\Table;
use \core\db\Database;

class Image extends Table {

    public static function importFromJson($data) {

        if (empty($data['calc0']) || empty($data['calc1'])) {
            throw new ImageException("invalid import");
        }

        $img = \imagecreatefrompng($data['calc0']);

        if (isset($data['calc1'])) {
            $calc_1 = \imagecreatefrompng($data['calc1']);
            \imagecopy($img, $calc_1, 0, 0, 0, 0, \imagesx($img), \imagesy($img));
            \imagedestroy($calc_1);
        }
        $res = new Image();
        
        \ob_start();
        \imagepng($img);

        $res->data = \ob_get_clean();
        \imagedestroy($img);
        return $res;
    }

    public function toDataUrl() {
        return 'data:image/png;base64,' . base64_encode($this->data);
    }

    public static function getFirstOrderByDate($amount) {
        $db = Database::getCursor();
        return $db->query('SELECT * FROM `image` ORDER BY `creation_date` DESC LIMIT ' . $amount, get_called_class(), null, false);
    }

    public static function getNextByTimeStamp($timestamp, $amount) {
        $db = Database::getCursor();
        return $db->query('SELECT * FROM `image` WHERE `creation_date` < ? LIMIT ' . $amount, get_called_class(), [$timesamp], false);
    }

    public static function getFirstOrderByAuthor($authorid, $amount) {
        $db = Database::getCursor();
        return $db->query('SELECT * FROM `image` WHERE `user_id`=? ORDER BY `creation_date` DESC LIMIT ' . $amount, get_called_class(), [$authorid], false);
    }

    public static function getAllOrderByAuthor($authorid) {
        $db = Database::getCursor();
        return $db->query('SELECT * FROM `image` WHERE `user_id`=? ORDER BY `creation_date` DESC', get_called_class(), [$authorid], false);
    }
}