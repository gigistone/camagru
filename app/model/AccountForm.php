<?php

namespace app\model;

class AccountForm {

    public $errors = [];

    public function validateUsername($name) {

        if (preg_match('#^[a-zA-Z0-9]{4,8}$#', $name, $arr) == 1) {
            return true;
        }
        return false;
    }

    public function validatePassword($password) {

        if (\preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,16}$#', $password, $arr) == 1) {
            return true;
        }
        return false;
    }

    public function validateEmail($mail) {
        if (\filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public function ValidateInput() {
        if ($this->validateUsername($_POST['username']) === false) {
            $this->errors[] = 'Username is incorrect (must contain between 4-8 alphanumerical characters)';
        }
        if ($this->validateEmail($_POST['mail']) === false) {
            $this->errors[] = 'Email is incorrect';
        }
        if ($this->validatePassword($_POST['password']) === false) {
            $this->errors[] = 'Password is inccorect (must contain at least one uppercase, one digit, and a total of 8-16 characters)';
        }
        else {
            if ($_POST['password'] !== $_POST['password_confirmation']) {
                $errors[] = 'Password and confirmation are different';
            }
        }
        if (count($this->errors) > 0) {
            return false;
        }
        return true;
    }
}