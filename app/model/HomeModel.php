<?php

namespace app\model;

use app\model\Image;

class HomeModel {

    private static $_images_per_request = 5;
    private static $_default_image_amount = 30;

    // public function getNextImagesFromID(int $id) {
    //     return Image::range($id, $_images_per_request);
    // }

    public static function getHomeImages() {
        return Image::getFirstOrderByDate(self::$_default_image_amount);
    }

    public static function getNextImageCollection($from_id) {
        return Image::rangeReverse($from_id, self::$_images_per_request);
    }
}