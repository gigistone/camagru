<?php

namespace app\model;

use \core\db\Table;
use \core\db\Database;

class Vote extends Table {

    public static function countByImageId($id) {
        $db = Database::getCursor();
        return $db->countQueryResults('SELECT COUNT(*) from `vote` where imageid=?', [$id]);
    }

    public static function FindByOwnerFromImage($imageid, $ownerid) {
        $db = Database::getCursor();

        return $db->query('SELECT * FROM `vote` WHERE ownerid=? AND imageid=?', 
            \get_called_class(), [$ownerid, $imageid], true)
        ;
    }
}