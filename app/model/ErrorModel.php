<?php

namespace app\model;

class ErrorModel {

    private $_imgList = [
        'fail',
        'fail2',
        'fail3'
    ];

    function RandomImgUrl() {
        return '/resources/img/' . $this->_imgList[rand(0, count($this->_imgList) - 1)] . '.gif';
    }
}