<?php

namespace app\model;

use core\db\Table;
use \core\db\Database;

class Comment extends Table {

    public static function findByImageId($id) {

        $db = Database::getCursor();

        return $db->query('SELECT * from `comment` WHERE imageid=?', get_called_class(), [$id], false);
    }
}