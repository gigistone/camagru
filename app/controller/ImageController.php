<?php
namespace app\controller;

use core\utils\Auth;
use core\utils\Redirect;
use core\utils\Input;

use app\model\Account;

use app\model\ImageException;
use app\model\Image;

use app\model\Comment;
use app\model\Vote;
use app\controller\ErrorController;

use core\utils\Session;
use core\utils\Url;

class ImageController extends AppController
{

    function create() {
        $json = file_get_contents('php://input');
        $json = \json_decode($json, true);
        $account = Session::getData('account');

        if ($account == null) {
            http_response_code(403);
            return ;
        }
        
        try {
            $img = Image::importFromJson($json);
        }
        catch (ImageException $ex) {
            http_response_code(403);
            return ;
        }

        $img->user_id = $account->id;
        $img->save();

        $json_response = [
            'url' => Url::fromRecord($img),
            'data' => base64_encode($img->data)
        ];

        echo json_encode($json_response);
    }

    function show($id) {

        if (!\filter_var($id, FILTER_VALIDATE_INT)) {
            http_response_code(404);
            return ;
        }

        $image = Image::find($id);

        if ($image == null) {
            $newPage = new ErrorController();
            $newPage->index(); 
            return ;
        }

        $image->comments = Comment::findByImageId($image->id);
        $account = Session::getData('account');
        $showRestrictedInfos = $account != null ? true : false;
        $image->ownerName = Account::find($image->user_id)->username;
        $image->countVotes = Vote::countByImageId($image->id);
        $voteChecked = false;

        if ($showRestrictedInfos) {
            $voteChecked =  Vote::FindByOwnerFromImage($image->id, $account->id) != null ? true : false;
        }

        foreach ($image->comments as $key => $value) {
            $image->comments[$key]->authorName = Account::find($value->authorid)->username;
        }

        if ($image == null) {
            echo 'pas d img';
            return ;
        }

        $this->view
            ->addTitle('Camagru')
            ->addScripts(['comment.js', 'vote.js'])
            ->render('image/show', compact('image', 'showRestrictedInfos', 'voteChecked'));
    }

    function get($id) {
        $img = Image::find($id);
        if ($img) {
            \header("Content-type: image/png");
            echo $img->data;
        }
        else {
            echo 'no img at this id';
        }
    }

    function delete($id) {

        if (!Auth::isAuth()) {
            http_response_code(403);
            return ;
        }

        if (!\filter_var($id, FILTER_VALIDATE_INT)) {
            http_response_code(403);
            return ;
        }

        $img = Image::find($id);
        $account = Session::getData('account');

        if ($img) {

            if ($img->user_id === $account->id)
                $img->delete();
            else {
                http_response_code(403);
            }
        }
        else {
            http_response_code(404);
        }
    }

    function showForUser($id) {

        if (empty($id)) {
            http_response_code(403);
            return ;
        }

        if (!\filter_var($id, FILTER_VALIDATE_INT)) {
            http_response_code(403);
            return ;
        }

        $account = Account::find($id);
        if ($account == null || !Auth::isAuth() || $account != Session::getData('account')) {
            $this->view
                ->addTitle('404')
                ->render('error/index');
                return ;
        }

        $images = Image::getAllOrderByAuthor($account->id);
        $this->view
            ->addTitle('Manage your images')
            ->addOneScript('image-manager.js')  
            ->render('user/images', compact('images'));
    }
}