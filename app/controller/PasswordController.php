<?php

namespace app\controller;
use app\model\Account;
use core\utils\Auth;
use core\utils\Redirect;
use core\utils\Input;
use core\utils\FlashBuilder;
use core\utils\Token;

class PasswordController extends AppController {

    function __construct() {
        parent::__construct();
        if (Auth::isAuth()) {
            Redirect::toUrl('/home');
        }
    }

    function index() {
        $this->view
            ->addTitle('Get my password ...')
            ->render('password/index');
    }

    function sendResetLink() {
        if (!isset($_POST['mail']) || empty($_POST['mail'])) {
            http_response_code(403);
            return ;
        }
        if (!\filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
            http_response_code(403);
            return ;
        }

        $flash = new FlashBuilder();
        $account = Account::findByEmail($_POST['mail']);

        if ($account == null) {
            $flash->addError('No account linked to this mail');
        }
        
        else if ($account->active == 0) {
            $flash->addError('Your account is not actived (check your mails)');
        }
        else {
            $account->token = Token::generate();
            $account->save();
            $url = $_SERVER['HTTP_HOST'] . "/password/reset/$account->username/$account->token";
            $this->view
                ->addTitle('Request new password')
                ->render('password/send_link');
                \mail($account->mail, 'reset your password', "click here bro: " . $url);
            return ;
        }


        $flash = $flash->toHtml();
        $this->view
            ->addTitle('Get your password ...')
            ->render('password/index', compact('flash'))
        ;
    }

    function edit($username, $token) {
        $account = Account::findByUsername($username);

        if ($account == null) {
            http_response_code(422);
            return ;
        }

        if ($account->token !== $token || $account->active == 0) {
            $this->view
                ->addTitle('Link_expired')
                ->render('password/link_expired');
            return ;
        }
        $this->view
            ->addTitle('Reset your password')
            ->render('password/edit', compact('account'));
    }

    function update($username, $token) {
        $account = Account::findByUsername($username);
        $allowChange = true;

        if ($account == null) {
            http_response_code(422);
            return ;
        }

        if ($account->token !== $token || $account->active == 0) {
            http_response_code(422);
            return ;
        }

        if (empty($_POST['password']) || empty($_POST['confirm_password'])) {
            http_response_code(422);
            return ;
        }

        $flash = new FlashBuilder();

        if (!$this->validatePassword($_POST['password'])) {
            $allowChange = false;
            $flash->addError('Password is inccorect (must contain at least one uppercase, one digit, and a total of 8-16 characters)');            
        }

        if ($_POST['password'] !== $_POST['confirm_password']) {
            $allowChange = false;
            $flash->addError('Password and password confirmation are different');
        }

        $this->view->addTitle('Password reset'); 

        if ($allowChange === true) {
            $account->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
            $account->token = "NULL";
            $account->save();

            $this->view->render('password/update');
            return ;
        }

        $flash = $flash->toHtml();
        $this->view->render('password/edit', compact('flash'));
    }

    private function validatePassword($password) {

        if (\preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,16}$#', $password, $arr) == 1) {
            return true;
        }
        return false;
    }
}