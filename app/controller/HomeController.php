<?php

namespace app\controller;

use app\model\Image;
use app\model\HomeModel;
use core\utils\Session;

class HomeController extends AppController {

    function index() {

        $images = HomeModel::getHomeImages();

        $last_id = !empty($images) ? end($images)->id : null;

        $show_header = Session::getData('account') == null ? true : false;

        $this->view
            ->addTitle('Welcolme to the faboulous world of Nitendo')
            ->addOneScript('home_pagination.js')
            ->render('home/index', compact('images', 'last_id', 'show_header'))
        ;
    }

    function paginate($from_id) {

        if (!\filter_var($from_id, FILTER_VALIDATE_INT)) {
            http_response_code(404);
            return ;
        }

        $images = HomeModel::getNextImageCollection($from_id);
        $response = new \stdClass();
        if ($images && \count($images) > 0) {

            $imgDataList = [];
            $response->lastId = end($images)->id;

            foreach($images as $img) {
                $imgDataList[] = [
                    'id' => $img->id,
                    'data' => $img->toDataUrl()
                ];
            }

            $response->imgList = $imgDataList;

            \header('Content-Type: application/json');
            echo json_encode($response);
        }
        else {
            $response->lastId = null;

            \header('Content-Type: application/json');
            echo json_encode($response);
        }
    }
}