<?php
namespace app\controller;

use core\utils\Auth;
use core\utils\Redirect;
use core\utils\Input;
use app\model\Account;
use core\utils\Session;

class EditorController extends AppController
{
    function __construct() {
        parent::__construct();
        if (!Auth::isAuth()) {
            Redirect::toUrl('/login');
        }
    }

    function index() {
        $account = Session::getData('account');
        $this->view->title = "Editor";
        $this->view->addOneScript('editor.js');
        $this->view->render('editor/index', compact('account'));
    }

}
