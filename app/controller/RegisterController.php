<?php

namespace app\controller;

use app\controller\ErrorController;
use \app\controller\AppController;
use \app\model\Account;
use \core\utils\Auth;
use \core\utils\Redirect;
use \core\utils\Token;
use \core\utils\FlashBuilder;

class RegisterController extends AppController
{
    function __construct() {
        parent::__construct();
        if (Auth::isAuth()) {
            Redirect::toUrl('/home');
        }
    }

    function index() {
        $this->view
            ->addTitle('Register')
            ->render('register/index');
    }

    function register() {
        $acc = new Account();


        if (!isset($_POST['username'])
        || !isset($_POST['mail'])
        || !isset($_POST['password'])
        || !isset($_POST['password_confirmation'])) {
            http_response_code(404);
            return ;
        }

        $flash = new FlashBuilder();
        
        if ($this->ValidateInput($flash) === true) {
            
            $exists = Account::findByUsername($_POST['username']);
            $mailExists = Account::findByEmail($_POST['mail']);
            if (!$exists && !$mailExists) {
                $acc->username =  $_POST['username'];
                $acc->mail = $_POST['mail'];
                $acc->password = $_POST['password'];
                $acc->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $acc->token = Token::generate();
                $acc->save();
                $this->sendActivationLink($acc);
                $this->view->render('register/send_mail');
                return ;
            }
            else {
                if ($exists) {
                    $flash->addError('Username already exists');
                }
                if ($mailExists) {
                    $flash->addError('The mail is already used');
                }
            }
        }
        $flash = $flash->toHtml();
        $this->view->render('register/index', \compact('flash'));
    }

    private function sendActivationLink($account) {
        $link = $_SERVER['HTTP_HOST'] . "/register/confirm/$account->username/$account->token"; 
        \mail($account->mail, 'Activate your Camagru account !', $link);
    }

    private function validateUsername($name) {

        if (preg_match('#^[a-zA-Z0-9]{4,8}$#', $name, $arr) == 1) {
            return true;
        }
        return false;
    }

    private function validatePassword($password) {

        if (\preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,16}$#', $password, $arr) == 1) {
            return true;
        }
        return false;
    }

    private function validateEmail($mail) {
        if (\filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    private function ValidateInput(&$flash) {
        $errors = false;
    
        if ($this->validateUsername($_POST['username']) === false) {
            $flash->addError('Username is incorrect (must contain between 4-8 alphanumerical characters');
            $errors = true;
        }
    
        if ($this->validateEmail($_POST['mail']) === false) {
            $flash->addError('Email is incorrect');
            $errors = true;
        }

        if ($this->validatePassword($_POST['password']) === false) {
            $flash->addError('Password is inccorect (must contain at least one uppercase, one digit, and a total of 8-16 characters)');
            $errors = true;
        }
        else {
            if ($_POST['password'] !== $_POST['password_confirmation']) {
                $flash->addError('Password and password confirmation are not the same');
                $errors = true;
            }
        }
        if ($errors === true) {
            return false;
        }
        return true;
    }

    public function activate($username, $token) {
        $account = Account::findByUsername($username);
        if ($account == null) {
            $errorPage = new ErrorController();
            $errorPage->index();
            return ;
        }

        if ($account->token !== $token || $account->active == 1) {
            $errorPage = new ErrorController();
            $errorPage->index();
            return ;
        }

        $account->token = 'NULL';
        $account->active = 1;
        $account->save();

        $this->view
            ->addTitle('Account activation')
            ->render('register/activation');
    }
}   