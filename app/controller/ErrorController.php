<?php

namespace app\controller;
use app\model\ErrorModel;

class ErrorController extends AppController {

    function index() {
        $errorInfos = new ErrorModel();
        $this->view
            ->addTitle('Oops ...')
            ->render('error/index', \compact('errorInfos'));
    }
}