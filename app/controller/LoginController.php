<?php
namespace app\controller;

use core\utils\Auth;
use core\utils\Redirect;
use core\utils\Input;
use app\model\Account;
use core\utils\Session;
use core\utils\FlashBuilder;

class LoginController extends AppController
{
    function __construct() {
        parent::__construct();
        if (Auth::isAuth()) {
            Redirect::toUrl('/home');
        }
    }

    function index() {

        $this->view
            ->addTitle('Login to Camagru')
            ->render('login/index');

    }

    function login() {
        if (!Input::exists(Input::REQUEST_POST)) {
            return ;
        }
        if (!Input::get('username') && !Input::get('password')) {
            return ;
        }
        $account = Account::findByUsername(Input::get('username'));
        $flash = new FlashBuilder();

        if ($account && $account->active == 1) {
            if (password_verify(Input::get('password'), $account->password)) {
                Auth::auth($account);
                Redirect::toUrl('/home');
            }
            else {
                $flash->addError('The password is wrong');
            }
        }
        else {
            $flash->addError('The account does not exist or is not active');
        }
        $flash = $flash->toHtml();
        $this->view->render('login/index', \compact('flash'));
    }

    function logout() {
        Session::destroy();
        Redirect::toUrl('/home');
    }
}
