<?php

namespace app\controller;

use \app\controller\AppController;
use \app\model\Account;
use \core\utils\Redirect;
use \core\View;

class UserController extends AppController
{
    function index() {

        $accounts = Account::all();

        $this->view
            ->addTitle('Camagru members !')
            ->render('user/index', compact('accounts'))
        ;

    }

    function show($id) {
        $account = Account::find($id);

        $this->view
            ->addTitle('Profile: ' . $account->username)
            ->render('user/show', compact('account'))
        ;
    }

    function delete($id) {
        $account = Account::find($id);
        Redirect::toRecord($account);
    }
}
