<?php

namespace app\controller;

use core\utils\Auth;
use core\utils\Session;
use app\model\AccountForm;
use app\model\Account;
use core\utils\FlashBuilder;

class AccountController extends AppController {


    function __construct() {
        parent::__construct();
        if (!Auth::isAuth()) {
            Redirect::toUrl('/login');
        }
        $this->view->addOneScript('user_settings.js');
    }

    function index() {

        $account = Session::getData('account');
        $this->view->addTitle('Camagru: Account settings');
        $this->view->render('account/index', compact('account'));
    }

    function update() {

        $account = Session::getData('account');

        if (!isset($_POST['username']) || !isset($_POST['mail'])
        || !isset($_POST['password']) || !isset($_POST['mail-comment'])) {
            http_response_code(404);
            return ;
        }

        $form = new AccountForm();
        $flash = new FlashBuilder();
        $account_changed = false;

        if (!empty($_POST['username'])) {
            if  ($form->validateUsername($_POST['username']) === true) {
                if (Account::findByUsername($_POST['username'])) {
                    $flash->addError('Username already exists');
                }
                else {
                    $account->username = $_POST['username'];
                    $account_changed = true;
                    $flash->addSuccess('Username has been updated');
                }
            }
            else {
                $flash->addError('Username is incorrect (must contain between 4-8 alphanumerical characters)');
            }
        }

        if (!empty($_POST['mail'])) {
            if ($form->validateEmail($_POST['mail'])) {
                $account->mail = $_POST['mail'];
                $account_changed = true;
                $flash->addSuccess('Email has been updated');
            }
            else {
                $flash->addError('The format of the email is incorrect');
            }
        }

        if (!empty($_POST['password'])) {
            if ($form->validatePassword($_POST['password'])) {
                $account->password = password_hash($_POST['password'], PASSWORD_DEFAULT);
                $account_changed = true;
                $flash->addSuccess('Password has been updated');
            }
            else {
                $flash->addError('Password is inccorect (must contain at least one uppercase, one digit, and a total of 8-16 characters)');
            }
        }
        if (!empty($_POST['mail-comment'])) {
            $status = $_POST['mail-comment'] === 'on' ? 1 : 0;
            if ($account->notify != $status) {
                $account->notify = $status;
                $account_changed = true;
                if ($status == 1) {
                    $flash->addSuccess('You enabled comments notification mails');
                }
                else {
                    $flash->addSuccess('You disabled comments notification mails');
                }
            }
        }

        if ($account_changed === true) {
            $account->save();
        }

        $flash = $flash->toHtml();

        $this->view->render('account/index', compact('account', 'flash'));
    }
}
