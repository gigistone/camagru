<?php

namespace app\controller;

use app\model\Account;
use app\model\Image;
use app\model\Comment;
use core\utils\Session;
use core\utils\Url;

class CommentController extends AppController {

    public function create($id) {

        if (empty($id)) {
            http_response_code(404);
            return ;
        }

        if (!\filter_var($id, FILTER_VALIDATE_INT)) {
            http_response_code(404);
            return ;
        }

        $img = Image::find($id);

        $account = Session::getData('account');
        if (!isset($img) || !isset($_POST['body']) || empty(trim($_POST['body']))) {
            http_response_code(404);
            return ;
        }

        $comment = new Comment();
        $comment->body = \htmlspecialchars($_POST['body']);
        
        if (\strlen($comment->body) < 160) {
            $comment->authorid = $account->id;
            $comment->imageid = $img->id;
            $comment->save();
            
            $response = [
                'error' => null,
                'author' => $account->username,
                'body' => $comment->body,
            ];
            $this->NotifyAuthor($account, $img);
        }
        else {
            $response = [
                'error' => 'The comment must be contains 160 characters max'
            ];
        }
        \header('Content-type: application/json');
        echo \json_encode($response);
    }

    private function NotifyAuthor($poster, $img) {
        $author = Account::find($img->user_id);
        $imgLink = Url::fromRecord($img);

        if ($author->notify == 0) {
            return ;
        }

        $subject = "You receive a new comment !";
        $body = "You receive a new comment on your image from $poster->username !
        Click on the link see comments :
        $imgLink";
        \mail($author->mail, $subject, $body);
    }
}
