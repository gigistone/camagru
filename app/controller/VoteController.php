<?php
namespace app\controller;

use core\utils\Auth;

use app\model\Account;
use app\model\Vote;
use app\model\Image;
use app\model\Comment;

use core\utils\Session;
use core\utils\Url;

class VoteController extends AppController {

    public function createForImage($id) {
        $account = Session::getData('account');
        
        if ($this->validateParams($id, $account) === false) {
            \http_response_code(404);
            return ;
        }

        if (Vote::FindByOwnerFromImage($id, $account->id) != null ) {
            \http_response_code(404);
            return ;
        }

        $vote = new Vote();
        $vote->imageid = $id;
        $vote->ownerid = $account->id;
        $vote->save();
    }

    public function deleteForImage($id) {
        $account = Session::getData('account');

        if ($this->validateParams($id, $account) === false) {
            \http_response_code(404);
            return ;
        }

        $vote = Vote::FindByOwnerFromImage($id, $account->id);
        
        if ($vote == null ) {
            \http_response_code(404);
            return ;
        }

        $vote->delete();
    }

    private function validateParams($imageid, $account) {

        if ($account == null || !\filter_var($imageid, FILTER_VALIDATE_INT)) {
            return false;
        }
        if ($img = Image::find($imageid) == null) {
            return false;
        }
        return true;
    }
}