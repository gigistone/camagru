<?php

namespace app;

class Autoloader 
{
    static function register() {
        spl_autoload_register(array(__CLASS__, 'load'));
    }

    static function load($classname) {
        if (strpos($classname, __NAMESPACE__) === 0) {
            $classname = str_replace(__NAMESPACE__ . '\\', '', $classname);
            $classname = str_replace('\\', '/', $classname);
            require $classname . '.php';
        }
    }
}
