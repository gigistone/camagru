<div id="login-container">
    <?php if(isset($flash) && !empty($flash)) echo $flash ?>
    <div id="signup-pop">
        <img src="/resources/img/mario_run.gif" alt="mario is running">
        <div class="nes-balloon from-left">
            <p>Or <a href="/register">signup now</a>!</p>
        </div>
    </div>
    <div class="nes-container is-dark with-title">
        <h2 class="title">Login</h2>
        <form class="form-group" action="/login" method="post">
            <input class="nes-input" type="text" name="username" required placeholder="Username"><br>
            <input class="nes-input" type="password" name="password" required placeholder="Password"><br>
            <a id="forget-password-link" href="/password/forget">I forget my password</a><br>
            <input class="nes-btn" type="submit" value="Login">
        </form>
    </div>
</div>