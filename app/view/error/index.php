<div class="center-page">
    <div class="nes-container is-dark with-title is-centered" style="max-width: 600px; height: auto; margin: 1rem;">
        <h1 class="title">404</h1>
        <img src="<?= $errorInfos->randomImgUrl() ?>" alt="" style="max-width: 100%; height: auto;">
        <div class="nes-container is-rounded">
            <p>This page is no longer available ...</p>
        </div>
    </div>
</div>