<?php 

use core\utils\Url; 

$placeRight = false;
?>

<div id="image-page">

        <div id="image-top-wrapper">
            <div class="image-content">
                <img src="<?= $image->toDataUrl() ?>">
            </div>
            <div class="image-infos nes-container is-dark">
                <img src="/resources/img/logo.png">
                <h4>Posted by <?= $image->ownerName ?></h4>
                <i class="nes-icon is-large heart"></i><span id="vote-counter"><?= $image->countVotes ?></span>
                <?php if($showRestrictedInfos === true): ?>
                <i id="vote-icon" title="love that pic" class="nes-icon is-large like <?php if ($voteChecked === false) echo 'is-empty' ?>"></i>
                <?php endif ; ?>
            </div>
        </div>
    <div class="comment-container nes-container with-title" data-requesturl="<?= Url::fromRecord($image) . '/comments/' ?>">
        <h4 class="title">Comments</h4>

        <div class="comment-list">
            <?php foreach ($image->comments as $comment) {
                if ($placeRight === true) {
                    echo  "
                        <section class=\"comment com-flex-right\">
                            <div class=\"nes-balloon from-right\">
                                <p>{$comment->body}</p>
                            </div>
                            <div class=\"comment-header\">
                                <p style=\"text-align: center;\">{$comment->authorName}</p>
                                <i class=\"nes-bcrikko\"></i>
                            </div>
                        </section>";
                }
                else {
                    echo  "
                        <section class=\"comment com-flex-left\">
                            <div class=\"comment-header\">
                                <p style=\"text-align: center;\">{$comment->authorName}</p>
                                <i class=\"nes-bcrikko\"></i>
                            </div>
                            <div class=\"nes-balloon from-left\">
                                <p>{$comment->body}</p>
                            </div>
                        </section>";
                }
                $placeRight = !$placeRight;
            }
            ?>
        </div>
    </div>
    <?php if($showRestrictedInfos): ?>
    <div id="new-comment-form" data-posright="<?= $placeRight === true ? 'true' : 'false' ?>">
        <div class="nes-container with-title is-dark">
            <h5 class="title">Add comment</h5>
            <textarea class="nes-container with-title" name="body" id="comment-content" cols="30" rows="10"></textarea><br>
            <button id="post-btn" class="nes-btn is-primary">Post</button>
        </div>
    </div>
    <?php endif ;?>
</div>