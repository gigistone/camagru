<?php use core\utils\Url; ?>

<div>
    <?php if ($show_header === true): ?>
    <div id="header-container">
        <div class="nes-container is-rounded is-centered">
            <h2>Live your passion for Nintendo</h2>
        </div>
        <p>Take a selfie with Mario, Pikachu, Bowser and many other characters from the Nintendo universe. Share your passion with crazy people like you!</p>
        <a class="nes-btn" href="/register">Register</a>
    </div>
    <?php endif ; ?>
    <div class="image-container" <?php if ($last_id != null) echo 'data-lastid="' . $last_id . '"' ?>>
        <?php if (empty($images)):?>
        <div class="image-card">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png">
        </div>
        <?php endif ; ?> 
        <?php foreach ($images as $img): ?>
        <div class="image-card">
            <a href="<?= Url::fromRecord($img) ?>"><img src="<?= $img->toDataUrl() ?>"></a>
        </div>
        <?php endforeach ; ?>
    </div>

</div>