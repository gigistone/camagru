<section id="editor-container">
    <div id="item-container">
        <img id="mario" data-sound="mario-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/mario.png" alt="">
        <img id="todd" data-sound="todd-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/todd.png" alt="">
        <img id="thug" data-sound="thug-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/thug.png" alt="">
        <img id="kong" data-sound="donkey-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/kong.png" alt="">
        <img id="bannana" class="draggable-img draggable-border" draggable="true" src="public/resources/img/banana.png" alt="">
        <img id="peach" data-sound="peach-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/daisy.png" alt="">
        <img id="yoshi" data-sound="yoshi-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/yoshi.png" alt="">
        <img id="kirby" class="draggable-img draggable-border" draggable="true" src="public/resources/img/kirby.png" alt="">
        <img id="link" class="draggable-img draggable-border" draggable="true" src="public/resources/img/link.png" alt="">
        <img id="pikachu" data-sound="pikachu-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/pikachu.png" alt="">
        <img id="pikaface" class="draggable-img draggable-border" draggable="true" src="public/resources/img/pikaface.png" alt="">
        <img id="pokeball" class="draggable-img draggable-border" draggable="true" src="public/resources/img/pokeball.png" alt="">
        <img id="masterball" class="draggable-img draggable-border" draggable="true" src="public/resources/img/masterball.png" alt="">
        <img id="carapuce" data-sound="carapuce-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/carapuce.png" alt="">
        <img id="megaman" class="draggable-img draggable-border" draggable="true" src="public/resources/img/megaman.png" alt="">
        <img id="metroid_casque" class="draggable-img draggable-border" draggable="true" src="public/resources/img/metroid_casque.png" alt="">
        <img id="salameche" data-sound="salameche-sound" class="draggable-img draggable-border" draggable="true" src="public/resources/img/salameche.png" alt="">
    </div>
    <div id="notice-container">
        <ul>
        <li>Drag item: Hold <kbd>Left click</kbd></li>
        <li>Grow item on pic: <kbd>Left click</kbd></li>
        <li>Shrink item on pic <kbd>Shift</kbd> + <kbd>Left click</kbd></li>
        <li>Delete item on pic <kbd>Alt</kbd> + <kbd>Left click</kbd></li>
        </ul>
        <div id="upload-image-container">
            <p>You don't own a webcam - want to upload your own image ?</p>
            <div class="upload-btn-wrapper">
                <button id="upload-btn" class="nes-btn">Upload a file</button>
                <input type="file" id="file_loader" name="myfile" accept=".png" />
            </div>
        </div>
    </div>
    <div id="edit-section">
        <div class="dropper-container">
            <div id="dropper" hidden>
                <video autoplay="true" id="videoElement"></video>
            </div>
        </div>
    <button class="nes-btn is-disabled" id="screenbutton" disabled>Prendre une photo</button>
    </div>
    <div id="img-history-container">
        <a href="<?= "/users/$account->id/images/" ?>" class="nes-btn is-primary" style="margin: 0.5rem 0.5rem 0.2rem 0.5rem;">Manage All pics</a>
        <h1>Last pics</h1>
    </div>
</section>

<audio id="carapuce-sound" src="/resources/audio/carapuce.mp3"></audio>
<audio id="donkey-sound" src="/resources/audio/donkey.mp3"></audio>
<audio id="pikachu-sound" src="/resources/audio/pikachu.mp3"></audio>
<audio id="mario-sound" src="/resources/audio/mario_success.mp3"></audio>
<audio id="salameche-sound" src="/resources/audio/salameche.mp3"></audio>
<audio id="todd-sound" src="/resources/audio/todd.mp3"></audio>
<audio id="peach-sound" src="/resources/audio/peach.mp3"></audio>
<audio id="yoshi-sound" src="/resources/audio/yoshi.mp3"></audio>
<audio id="thug-sound" src="/resources/audio/thug.mp3"></audio>