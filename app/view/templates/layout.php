<?php use app\App; ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://unpkg.com/nes.css@2.1.0/css/nes.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/resources/style.css">
    <title><?= $view->title ?></title>
  </head>

  <body>
    <?php require App::ROOT . '/view/templates/nav.php'; ?>
    <main>
        <?= $view->content ?>
    </main>
    <footer>
      <span style="font-size: 0.5rem;">Copyrigth bboutoil incorporated unlimited edge found partnership subzero</span>
    </footer>
    <?php foreach ($view->scripts as $script): ?>
        <script type="text/javascript" src="<?= '/resources/scripts/' . $script ?>"></script>
    <?php endforeach; ?>
  </body>
</html>