<?php use core\utils\Auth; ?>

<section class="top-nav">
    <a href="/home" id="nav-header">
        <i class="nes-logo"></i>
        <span style="font-size: 1.5rem; vertical-align: middle;">Camagru</span>
    </a>
    <input id="menu-toggle" type="checkbox" />
    <label class='menu-button-container' for="menu-toggle">
        <div class='menu-button'></div>
    </label>
    <ul class="menu">
        <?php if (Auth::isAuth()): ?>
        <li><a href="/editor">Editor</a></li>
        <li><a href="/account">Account</a></li>
        <li><a href="/logout">Logout</a></li>
        <?php else : ?>
        <li><a href="/login">Login</a></li>
        <?php endif ; ?>
    </ul>
</section>