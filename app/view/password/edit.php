<section>
    <div id="newpass-container">
        <img id="newpass-content-img" src="/resources/img/mario_key.gif" alt="">
        <?php if(!empty($flash)) echo $flash; ?>
        <form class="nes-container is-dark" action="" method="post">
                <input class="nes-input" type="password" name="password" title="a password with a minimum of 8 characters : at least one uppercase letter required and one digit required" placeholder="new password" required><br>
                <input class="nes-input" type="password" name="confirm_password" placeholder="confirm password" required>
            <input class="nes-btn is-primary" type="submit" value="Change password"><br>
        </form>
    </div>
</section>