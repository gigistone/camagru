<section class="center-page">
    <div class="nes-container is-dark" style="text-align: center;">
        <p class="nes-text is-warning">It seems your link is expired ...</p>
        <a href="/password/forget">Request a new password link</a>
    </div>
</section>