<section class="center-page">
    <div class="nes-container is-dark is-rounded" style="text-align: center;">
        <p class="nes-text is-success">A password reset link has been sent to your address mail</p>
        <p class="nes-text is-primary">Check your mails and follow the instructions !</p>
    </div>
</section>