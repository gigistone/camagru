<section class="center-page">
    <div id="forgotpass-container">
        <?php if(!empty($flash)) echo $flash; ?>
        <form class="nes-container is-dark" action="/password/forget" method="post">
            <label> Email
                <input class="nes-input" type="email" name="mail" required>
            </label><br>
            <input class="nes-btn is-primary" type="submit" value="Reset password"><br>
        </form>
    </div>
</section>