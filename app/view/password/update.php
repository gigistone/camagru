<section class="center-page">
    <div class="nes-container is-dark is-rounded">
        <p class="nes-text is-success">Your password has been changed. You can now login with it.</p>
    </div>
</section>