<?php use core\utils\Url; ?>

<div id="image-manager">
    <?php foreach ($images as $img): ?>
        <div class="image-manage-card" data-image-id="<?= $img->id ?>">
            <a href="<?= Url::fromRecord($img) ?>"><img src="<?= $img->toDataUrl() ?>"></a>
            <i class="fas fa-times close-icon"></i>
        </div>
    <?php endforeach ; ?>
</div>

<!-- 
    function createPictureFromJsonresponse(json) {
    const imgInfos = JSON.parse(json);
    const img = document.createElement('img');
    const container = document.createElement('div');
    container.style.position = 'relative';
    container.classList.add('img-history-card');
    const i = document.createElement('i');
    
    img.src = 'data:image/png;base64,' + imgInfos.data;
    i.classList.add("fas", "fa-times", "close-icon");

    i.addEventListener('click', () => {
        const request = new XMLHttpRequest();
        request.onload = () => {
            if (request.status === 200) {
                container.remove();
            }
        };
        request.open('DElETE', imgInfos.url);
        request.send(null);
    });

    container.appendChild(img);
    container.appendChild(i);
    img_history.appendChild(container);
} -->