<div id="account-container">
    <img src="resources/img/poke_nurse.gif" alt="">
    <?php if(isset($flash) && !empty($flash)) echo $flash ?>
    <div id="account-general-form-container" class="nes-container is-dark with-title">
        <h4 class="title">General settings</h4>
        <form id="update-account-form" name="update-account" action="/account" method="post">
        <label title="A username contains alphanumeric values and length between 4-8">Username
            <input class="nes-input account-field" type="text" name="username" placeholder="<?= $account->username ?>">
        </label>
        <br>
        <label title="a valid email address">Mail address
            <input class="nes-input account-field" type="email" name="mail" id="" placeholder="<?= $account->mail ?>">
        </label>
        <br>
        <label title="a password which contains at least 8 characters, [0-9][A-Z][a-z]">New password
            <input id="password-field" class="nes-input" type="password" name="password" id="" placeholder="new password" autocomplete="off">
        </label>
        <br>
        <label title="confirm your password">Password confirmation
            <input id="confirm-password-field" class="nes-input" type="password" name="confirm_password" id="" placeholder="confirm password" autocomplete="off">
        </label>
        <br>
        </div>
    <div id="notification-form-container" class="nes-container is-dark with-title">
        <h4 class="title">Notifications</h4>
        <label>
            <input id="mail-notif-checkbox" type="checkbox" class="nes-checkbox is-dark account-field" name="mail-comment" value="on" <?php if($account->notify == 1) echo 'checked' ?>/>
            <span>Send me an email when i received a comment</span>
        </label>    
        <input id="mail-comment-toggler" type="hidden" name="mail-comment" value="off" />
    </div>
    <input id="update-btn" class="nes-btn" type="submit" value="Update">
    </form>
</div>