<section class="center-page">
    <div class="nes-container is-dark">
        <p class="nes-text is-success">Your account is now active. You can now login and enjoy Camagru !!</p>
    </div>
</section>