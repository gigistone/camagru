<div class="center-page">
    <div class="nes-container is-dark" style="max-width: 800px; margin: 1rem;">
        <p style="text-align: justify;">An activation link has been sent to your email address. Please validate your account by clicking on the link displayed in this email.
        </p>
    </div>
</div>