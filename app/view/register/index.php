<div id="register-container">
    <!-- <img src="resources/img/money_register.jpg" alt=""> -->
    <div class="nes-container is-rounded is-dark">
        <p>Don't forget to thank Mr. Miyamoto!</p>
    </div>
    <?php if (isset($flash) && !empty($flash)) echo $flash; ?>
    <div id="register-form-container" class="nes-container is-dark with-title is-centered">
        <h4 class="title">Register</h4>
        <?php if(isset($errors)) foreach($errors as $err): ?>
            <span class="nes-text is-error"><?= $err ?></span><br><br>
        <?php endforeach ; ?>
            <form class="" action="/register" method="post"><br>
        <input title="a valid email address" class="nes-input" type="email" name="mail" placeholder="Email" required><br>
        <input title="username contains at least 4 characters to 8. digits and letters are authorized" class="nes-input" type="text" name="username" placeholder="Username" required><br>
        <input title="a password with a minimum of 8 characters : at least one uppercase letter required and one digit required" class="nes-input" type="password" name="password" placeholder="Password" required autocomplete="off"><br>
        <input title="confirm your password" class="nes-input" type="password" name="password_confirmation" placeholder="Confirm your Password" required autocomplete="off"><br>
        <input class="nes-btn" type="submit" value="register">
    </div>
    </form>
</div>