function trim (s, c) {
    if (c === "]") c = "\\]";
    if (c === "\\") c = "\\\\";
    return s.replace(new RegExp(
      "^[" + c + "]+|[" + c + "]+$", "g"
    ), "");
}

let voteNode = document.querySelector('#vote-icon');
let voteCounter = document.querySelector('#vote-counter');

if (voteNode) {
    voteNode.addEventListener('click', () => {
        let req_url = trim(window.location.pathname, '\\') + '/like';
        let method = 'DELETE';
        
        let req = new XMLHttpRequest();
        req.onload = () => {
            if (req.status === 200) {
                voteNode.classList.toggle('is-empty');
                let votes = parseInt(voteCounter.textContent);
                if (voteNode.classList.contains('is-empty')) {
                    votes -= 1;
                }
                else {
                    votes += 1;
                }
                voteCounter.textContent = votes;
            }
        };
        if (voteNode.classList.contains('is-empty')) {
            method = 'POST';
        }
        req.open(method, req_url);
        req.send(null);
    });
}