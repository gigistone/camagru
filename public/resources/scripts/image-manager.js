const closeIcons = document.querySelectorAll('.image-manage-card i');

closeIcons.forEach(icon => {
    icon.addEventListener('click', () => {
        
        const container = icon.parentElement;
        const request = new XMLHttpRequest();
        request.onload = () => {
            if (request.status === 200) {
                container.remove();
            }
        };
        request.open('DElETE', `/images/${container.getAttribute('data-image-id')}`);
        request.send(null);
    })
});