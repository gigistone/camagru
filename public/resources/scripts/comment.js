let commentList = document.querySelector('.comment-list');
let req_url = document.querySelector('.comment-container').getAttribute('data-requesturl');
let postBtn = document.querySelector('#post-btn');
let commentPlaceRight = null;

if (postBtn) {
    commentPlaceRight = document.querySelector('#new-comment-form').getAttribute('data-posright');
    commentPlaceRight = commentPlaceRight === 'true' ? true : false; 
}

function createComment(commentData) {

    let comment = document.createElement('section');
    comment.classList.add('comment');
    comment.setAttribute('data-id', commentData.id);

    commentData.body = commentData.body.replace("\n", "<br>");

    if (commentPlaceRight === true) {
        comment.classList.add('com-flex-right');
        comment.innerHTML = `
            <div class="nes-balloon from-right">
                <p>${commentData.body}</p>
            </div>
            <div class="comment-header">
                <p style="text-align: center;">${commentData.author}</p>
                <i class="nes-bcrikko"></i>
            </div>`;
    }
    else {
        comment.classList.add('com-flex-left');
        comment.innerHTML = `
            <div class="comment-header">
                <p style="text-align: center;">${commentData.author}</p>
                <i class="nes-bcrikko"></i>
            </div>
            <div class="nes-balloon from-left">
                <p>${commentData.body}</p>
            </div>`;
    }

    commentPlaceRight = !commentPlaceRight;
    commentList.appendChild(comment);
}

if (postBtn != null) {
    
    document.querySelector('#post-btn').addEventListener('click', () => {

        let content = document.querySelector('#comment-content').value;
        let req = new XMLHttpRequest();

        if (content.length > 160) {
            alert('160 chars per comment bro');
            return ;
        }

        req.onload = () => {

            if (req.status === 200) {
                let commentData = JSON.parse(req.responseText);

                if (commentData.error) {
                    alert(commentData.error);
                    return ;
                }

                document.querySelector('#comment-content').value = "";
                createComment(commentData);
            }
        };

        req.open('POST', req_url, true);
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        req.send(`body=${content}`);
    });
}
