const startbutton  = document.querySelector('#screenbutton');
const camera = document.querySelector('#videoElement');
let capture = document.querySelector('#videoElement');
const dragItems = document.querySelectorAll('*[draggable=true]');
const dropZone = document.querySelector('#dropper');
const captureMode = 'webcam';
const img_history = document.querySelector('#img-history-container');
const img_loader = document.querySelector('#file_loader');
let itemCount = 0;

if (navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia({ video: { width: 640, height: 480 }, audio: false })
    .then(function (stream) {
        camera.srcObject = stream;
        dropZone.removeAttribute('hidden');

        })
    .catch(function (err) {
    });
}

/* DRAG ITEMS */

dragItems.forEach((item) => {

    item.addEventListener('dragstart', (e) => {
        e.target.classList.toggle('draggable-border');
        e.dataTransfer.setData('text', e.target.id);
        e.effectAllowed = 'copy';
    });

    item.addEventListener('dragend', (e) => {
        e.target.classList.toggle('draggable-border');
    });

});


/* DROPZONE */

dropZone.addEventListener('dragover', (e) => {
    e.preventDefault();
});

dropZone.addEventListener('dragenter', (e) => {
    dropZone.style.border = '3px dashed red';
});

dropZone.addEventListener('dragleave', (e) => {
        dropZone.style.border = 'none';
});

dropZone.addEventListener('drop', (e) => {
    e.preventDefault();
    dropZone.style.border = 'none';
    
    const img = document.getElementById(e.dataTransfer.getData('text'));
    
    if (img === null)
    return ;
    const new_img = document.createElement('img');

    new_img.src = img.src;
    new_img.width = img.width;
    new_img.height = img.height;
    new_img.style.position = 'absolute';
    new_img.style.top = (e.layerY - img.height / 2) + 'px';
    new_img.style.left = (e.layerX - img.width / 2) + 'px';
    new_img.classList.add('on_screen');

    if (++itemCount === 1) {
        ToggleBtnState(true);
    }


    new_img.addEventListener('click', (e) => {

        if(e.shiftKey) {
            new_img.style.top = new_img.offsetTop + 5 + 'px';
            new_img.style.left = new_img.offsetLeft + 5 + 'px';
            new_img.width -= 10;
            new_img.height -= 10;
        }
        else if(e.altKey) {
            new_img.remove();
            if (--itemCount === 0) {
                ToggleBtnState(false);
            }
        }
        else {
            new_img.style.top = new_img.offsetTop - 5 + 'px';
            new_img.style.left = new_img.offsetLeft - 5 + 'px';
            new_img.width += 10;
            new_img.height += 10;
        }
    })
    dropZone.appendChild(new_img);

    if (img.getAttribute('data-sound') != null) {
        document.getElementById(img.getAttribute('data-sound')).play();
    }
});

// TAKE PICTURE

startbutton.addEventListener('click', (ev) => {

    if (document.querySelectorAll('.on_screen').length === 0) {
        return ;
    }

    const data = {};
    const canv = document.createElement('canvas');

    // get original width of camera or webcam.
    // we need original width to position the items correctly if window was resize (responsive)

    const originalWidth = capture.nodeName === 'VIDEO' ? capture.videoWidth : capture.naturalWidth;
    const originalHeight = capture.nodeName === 'VIDEO' ? capture.videoHeight : capture.naturalHeight;
    const ratio = originalWidth / capture.clientWidth;

    canv.width = originalWidth;
    canv.height = originalHeight;

    canv.getContext('2d')
        .drawImage(capture, 0, 0, originalWidth, originalHeight);
    data['calc0'] = canv.toDataURL('image/png');

    canv.getContext('2d').clearRect(0, 0, originalWidth, originalHeight);

    document.querySelectorAll('.on_screen').forEach((item) => {

        let leftOffset;
        let topOffset;

        leftOffset = item.offsetLeft * originalWidth / capture.clientWidth;
        topOffset = item.offsetTop * originalHeight / capture.clientHeight;

        canv.getContext('2d')
            .drawImage(item, leftOffset, topOffset, item.width * ratio, item.height * ratio);
    });
    data['calc1'] = canv.toDataURL('image/png');

    const request = new XMLHttpRequest();
    request.onload = () => { 
        if (request.status == 200) {
            createPictureFromJsonresponse(request.responseText);
        }
    };
    request.open('POST', '/images');
    request.setRequestHeader('Content-Type', 'application/json');
    request.send(JSON.stringify(data));
    ev.preventDefault();
});

function createPictureFromJsonresponse(json) {
    const imgInfos = JSON.parse(json);
    const img = document.createElement('img');
    const container = document.createElement('div');
    container.style.position = 'relative';
    container.classList.add('img-history-card');
    const i = document.createElement('i');
    
    img.src = 'data:image/png;base64,' + imgInfos.data;
    i.classList.add("fas", "fa-times", "close-icon");

    i.addEventListener('click', () => {
        const request = new XMLHttpRequest();
        request.onload = () => {
            if (request.status === 200) {
                container.remove();
            }
        };
        request.open('DElETE', imgInfos.url);
        request.send(null);
    });

    container.appendChild(img);
    container.appendChild(i);
    img_history.appendChild(container);
}

img_loader.addEventListener('change', () => {

    if (img_loader.files[0].type !== 'image/png')
        return ;

    const img = document.createElement('img');
    capture = img;
    img.src = window.URL.createObjectURL(img_loader.files[0]);
    img.style.maxWidth = '100%';
    img.style.height = 'auto';
    dropZone.removeAttribute('hidden');
    dropZone.firstElementChild.remove();
    dropZone.appendChild(img);

});

function ToggleBtnState(state) {
    if (state === false) {
        startbutton.disabled = true;
        startbutton.classList.add('is-disabled');
        startbutton.classList.remove('is-primary');
    }
    else {
        startbutton.disabled = false;
        startbutton.classList.remove('is-disabled');
        startbutton.classList.add('is-primary');
    }
}