let imgContainer = document.querySelector('.image-container');
let last_id = imgContainer.getAttribute('data-lastid');
let can_request =  last_id == null ? false : true;

function onJsonReceived(json) {

    let imgRaw = JSON.parse(json);
    last_id = imgRaw.lastId;
    if (last_id == null) {
        can_request = false;
        return ;
    }

    imgRaw.imgList.forEach(img => {
        let imgCard = document.createElement('div');
        imgCard.classList += 'image-card';

        imgCard.innerHTML = `
        <a href="/images/${img.id}"><img src="${img.data}"></a>`;
        imgCard.src = img.data;
        imgContainer.appendChild(imgCard);
    });

}

// function 

window.addEventListener('scroll', () => {

    if (can_request === false)
        return ;

    if (window.scrollY >= (document.documentElement.scrollHeight - document.documentElement.clientHeight)) {
            let req = new XMLHttpRequest();

            req.onload = () => {
                if (req.status === 200) {
                    onJsonReceived(req.responseText);
                }
            };
            req.open('GET', '/home/paginate/' + last_id, true);
            req.send(null);
    }
});