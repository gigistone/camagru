function validate_password(pass, confirm) {
    pass = pass.value;
    confirm = confirm.value;

    if (pass || confirm) {
        if (pass !== confirm) {
            return false;
        }
        else {
            return true;
        }
    }
    return false;
}

document.getElementById('update-btn').addEventListener('click', () => {
    if (document.getElementById('mail-notif-checkbox').checked) {
        document.getElementById('mail-comment-toggler').disabled = true;
    }
});

document.getElementById('update-account-form').addEventListener('submit', (e) => {
    let pass = document.querySelector('#password-field');
    let confirm_pass = document.querySelector('#confirm-password-field');
    let pass_valid = validate_password(pass, confirm_pass);

    if ((pass.value !== '' || confirm_pass.value !== '') && pass_valid === false) {
        pass.classList.add('is-error');
        confirm_pass.classList.add('is-error');
        e.preventDefault();
        return ;
    }
    else {
        pass.classList.remove('is-error');
        confirm_pass.classList.remove('is-error');
    }
    return ;
});