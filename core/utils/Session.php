<?php

namespace core\utils;

class Session
{
    static function init() {
        if (empty(session_id())) {
            \session_start();
        }
    }

    static function getData($key) {
        if (isset($_SESSION[$key]))
            return $_SESSION[$key];
        return null;
    }

    static function setData($key, $value) {
        $_SESSION[$key] = $value;
    }

    static function destroy() {
        \session_destroy();
    }
}
