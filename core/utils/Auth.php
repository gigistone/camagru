<?php

namespace core\utils;

class Auth
{
    static function isAuth() {
        Session::init();
        if (Session::getData('account') == null) {
            Session::destroy();
            return false;
        }
        return true;
    }

    static function Auth(\app\model\Account $account) {
        Session::init();
        Session::setData('account', $account);
    }
}
