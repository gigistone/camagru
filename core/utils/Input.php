<?php
namespace core\utils;

class Input {


    const REQUEST_GET = 'GET';
    const REQUEST_POST = 'POST';

    static function exists($req_type) {
        if ($_SERVER['REQUEST_METHOD'] == $req_type) {
            return true;
        }
        return false;
    }

    static function get($key) {
        // switch ($_SERVER['REQUEST_METHOD']) {
        //     case Input::REQUEST_GET :
        //         return $_GET[$key];
        //     case Input::REQUEST_POST :
        //         return $_POST[$key];
        //     default:
        //         return null;
        // }
        return $_REQUEST[$key];
    }

}
