<?php

namespace core\utils;

use core\controller\Table;
use core\utils\Url;

class Redirect {

    static function toUrl($url) {

        Header('Location: ' . $url);
    }

    static function toRecord($record) {

        $content = Url::fromRecord($record);
        header('Location: ' . $content);
    }

}
