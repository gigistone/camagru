<?php

namespace core\utils;

abstract class Token {

    static function generate() {
        return bin2hex(openssl_random_pseudo_bytes(16));
    }
}