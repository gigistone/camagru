<?php

namespace core\utils;

class Url
{
    static function fromRecord($record) {

        $record_reflex = new \ReflectionClass($record);
        $controller = \strtolower($record_reflex->getShortName()) . 's';

        return 'http://' . $_SERVER['HTTP_HOST']
                .'/' . $controller . '/' . $record->id;
    }
}
