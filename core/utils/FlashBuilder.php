<?php

namespace core\utils;

class Flash {

    public $type;
    public $text;

    public function __construct($type, $text) {
        $this->type = $type;
        $this->text = $text;
    }
}

class FlashBuilder {
    
    private $flashList = [];

    public function addWarning($text) {
        return $this->_addFlash('is-warning', $text);
    }

    public function addError($text) {
        return $this->_addFlash('is-error', $text);
    }

    public function addSuccess($text) {
        return $this->_addFlash('is-success', $text);
    }

    private function _addFlash($type, $text) {
        $this->flashList[] = new Flash($type, $text);
        return $this;
    }

    public function toHtml() {
        $html = '';

        foreach ($this->flashList as $flash) {
            $html = $html . "<div class=\"nes-container is-rounded is-dark flash\">
                    <p class=\" flash-content nes-text $flash->type\">$flash->text</p>
            </div>";
        }
        return $html;
    }
}