<?php

namespace core;

use app\App;

class View
{

    private static $viewPath = App::ROOT .  '/view/';

    private $layout = 'layout';
    private $navPath = 'nav';
    public $title = '';
    private $css = [];
    private $scripts = [];
    private $description;
    public $content;
    public $nav;
    private $show_nav = true;

    public function __construct() {}

    function addLayout($layout) {
        $this->layout = $layout;
        return $this;
    }

    function addTitle($title) {
        $this->title = $title;
        return $this;
    }

    function addDescription($desc) {
        $this->description = $desc;
        return $this;
    }

    function addCss($css) {
        $this->css = $css;
        return $this;
    }

    function addScripts(array $scripts) {
        $this->scripts = $scripts;
        return $this;
    }

    function addOneScript($script) {
        $this->scripts[] = $script;
        return $this;
    }

    function addnav($nav) {
        $this->nav = $nav;
        return $this;
    }

    function hideNav() {
        $this->show_nav = false;
        return $this;
    }

    function render($template, array $args = null) {
        if ($args != null) {
            extract($args);
        }

        ob_start();
        require self::$viewPath . $template . '.php';
        $this->content = ob_get_clean();

        if ($this->show_nav) {
            ob_start();
            require self::$viewPath . 'templates/' . $this->navPath . '.php';
            $this->nav = ob_get_clean();
        }

        $view = $this;
        require self::$viewPath . 'templates/' . $this->layout . '.php';
    }
}
