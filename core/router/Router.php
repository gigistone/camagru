<?php

namespace core\router;

class Router
{

    private $routes = [];
    private $url;

    function __construct($url) {
        $this->url = $url;
    }

    function get($path, $callable) {
        return $this->addRoute($path, $callable, 'GET');
    }

    function post($path, $callable) {
        return $this->addRoute($path, $callable, 'POST');
    }

    function delete($path, $callable) {
        return $this->addRoute($path, $callable, 'DELETE');
    }

    function addRoute($path, $callable, $req_method) {
        $route = new Route($path, $callable);
        $this->routes[$req_method][] = $route;
        return $route;
    }

    function run() {

        if(!isset($this->routes[$_SERVER['REQUEST_METHOD']])) {
            throw new RouterException('Request Method isnt known');
        }

        foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route) {
            if ($route->match($this->url))
                return $route->call();
        }

        throw new RouterException('No valid routes: ' . $this->url);
    }
}
