<?php

namespace core\db;

use \PDO;   
use \PDOException;

class Database {
    
    private static $instance = null;
    
    /**
     * @var PDO
     */
    private $cursor;
    
    private function __construct() {
        require_once __DIR__ . '/../../config/database.php';
        try {
            $this->cursor = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD, $DB_OPTIONS);
        }
        catch (PDOException $ex) {
            echo ex;
            die();
        }
    }

    static function getCursor() {
        if (self::$instance == null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    function close() {
        $this->cursor = null;
    }

    function query($statement, $class, $attributes = null, $single = false) {
        
        $res = null;
        try {
            if ($attributes == null) {
                $res = $this->cursor->query($statement);
            }
            else {
                $res = $this->cursor->prepare($statement);
                $res->execute($attributes);
            }
        }
        catch (PDOException $ex) {
            echo $ex;
            die();
        }
        $res->setFetchMode(PDO::FETCH_CLASS, $class);
        if ($single == true) {
            return $res->fetch();
        }
        return $res->fetchAll();
    }

    function nonQuery($statement, $attributes = null) {
        try {
            if ($attributes == null) {
                $res = $this->cursor->query($statement);
            }
            else {
                $res = $this->cursor->prepare($statement);
                $res->execute($attributes);
            }
        }
        catch (PDOException $ex) {
            echo $ex;
            die();
        }
    }

    function lastInsertID() {
        return $this->cursor->lastInsertId();
    }

    function getRowCount($table) {
        $res = $this->cursor->query('SELECT COUNT(*) from ' . $table);
        return $res->fetchColumn();
    }

    function countQueryResults($statement, $attributes = null) {
        
        $res = null;
        try {
            if ($attributes == null) {
                $res = $this->cursor->query($statement);
            }
            else {
                $res = $this->cursor->prepare($statement);
                $res->execute($attributes);
            }
        }
        catch (PDOException $ex) {
            echo $ex;
            die();
        }
        return $res->fetchColumn();
    }
}
