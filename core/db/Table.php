<?php

namespace core\db;

use \core\db\Database;

/**
 * Represent a table in the database
 */
abstract class Table
{
    public $id;
    private static function getTableName() {

        $res = explode('\\', get_called_class());
        return \strtolower(end($res));;
    }

    /**
     * all
     *
     * Get All entities of this class representing in the Database
     *
     * @return object
     **/
    public static function all() {
        $db = Database::getCursor();
        return $db->query('SELECT * FROM ' . self::getTableName(), get_called_class());
    }

    // public static function new() {
    //     $class = get_called_class();
    //     return new $class();
    // }

    public static function find($id) {

        $db = Database::getCursor();
        if (!is_array($id)) {
            return $db->query('SELECT * FROM ' . self::getTableName()
            . ' WHERE id=' . $id , get_called_class(), null, true);
        }
        else {
            $results = [];
            foreach ($id as $cur_id) {
                $res = $db->query('SELECT * FROM ' .
                self::getTableName() . ' WHERE id = ' .
                $cur_id , get_called_class());
                if ($res != null) {
                    $results[] = $res;
                }
            }
            return $results;
        }
    }

    public static function first($nb = null) {
        
        $db = Database::getCursor();
        
        if ($nb === null || $nb < 1) {
            return $db->query('SELECT * FROM ' . self::getTableName()
            . ' LIMIT 1' , get_called_class(), null, true);
        }
        else if ($nb > 1) {
            return $db->query('SELECT * FROM ' . self::getTableName()
            . ' LIMIT ' . $nb , get_called_class(), null, false);
        }
    }

    public function save() {
        if (isset($this->id)) {
            $this->update();
        }
        else {
            $this->insert();
        }
    }

    private function insert () {
        $columns = [];
        $values = [];
        $val_str = "";
        
        foreach ($this as $k => $v) {
            if (isset($v)) {
                $columns[] = $k;
                $values[] = $v;
                $val_str = $val_str . "?,";
            }
        }

        $val_str = rtrim($val_str, ',');
        $query = 'INSERT INTO ' 
                    . self::getTableName() 
                    . ' ('
                    . \join(', ', $columns)
                    . ') VALUES ('
                    . $val_str
                    .')';
        $db = Database::getCursor();
        $db->nonQuery($query, $values);
        $this->id = $db->lastInsertID();
    }

    private function update() {

        $val_str = "";
        $values = [];

        foreach ($this as $k => $v) {
            if (isset($v)) {
                $val_str = $val_str . $k . ' = ?, ';
                $values[] = $v;
            }
        }
        $values[] = $this->id;

        $val_str = rtrim($val_str, ', ');
        $query = 'UPDATE ' 
                    . self::getTableName() 
                    . ' SET '
                    . $val_str
                    . ' WHERE id = ?';
        $db = Database::getCursor();
        $db->nonQuery($query, $values);
    }

    public function delete() {

        $query = 'DELETE FROM ' 
                    . self::getTableName()
                    . ' WHERE id = ' . $this->id
        ;

        $db = Database::getCursor();
        $db->nonQuery($query);
    }

    public static function count() {
        $db = Database::getCursor();
        return $db->getRowCount(self::getTableName());
    }

    public static function range($start_id, $amount) {
        $db = Database::getCursor();
        return $db->query('SELECT * FROM ' . self::getTableName()
            . ' WHERE id > ?'
            . ' LIMIT ' . $amount, get_called_class(), [$start_id], false);
    }

    public static function rangeReverse($start_id, $amount) {
        $db = Database::getCursor();
        return $db->query('SELECT * FROM ' . self::getTableName()
            . ' WHERE id < ? ORDER BY id DESC'
            . ' LIMIT ' . $amount, get_called_class(), [$start_id], false);
    }
}
