<?php

require_once '../app/Autoloader.php';
require_once '../core/Autoloader.php';

// Load Autoloaders
\app\Autoloader::register();
\core\Autoloader::register();

use \core\router\Router;
use \core\router\RouterException;
use \app\controller\ErrorController;

if (empty($_GET['url'])) {
    $_GET['url'] = '/home';
}

$router = new Router($_GET['url']);


$router->get('/home', 'Home#index');
$router->get('/home/paginate/:id', 'Home#paginate');

$router->get('/users/:id/images/', 'Image#showForUser');
$router->get('/users/:id', 'User#show');
$router->get('/users', 'User#index');

$router->get('/register/confirm/:username/:token', 'Register#activate');
$router->get('/register', 'Register#index');
$router->post('/register', 'Register#register');

$router->get('/password/reset/:username/:token', 'Password#edit');
$router->post('/password/reset/:username/:token', 'Password#update');
$router->post('/password/forget', 'Password#sendResetLink');
$router->get('/password/forget', 'Password#index');
$router->get('/login', 'Login#index');
$router->post('/login', 'Login#login');
$router->get('/logout', 'Login#logout');

$router->get('/editor', 'Editor#index');

$router->delete('images/:id/like', 'Vote#deleteForImage');
$router->post('images/:id/like', 'Vote#createForImage');
$router->post('images/:id/comments', 'Comment#create');
$router->get('images/:id', 'Image#show');
$router->post('/images', 'Image#create');

$router->get('/images/:id', 'Image#show');
$router->delete('/images/:id', 'Image#delete');

$router->get('/account', 'Account#index');
$router->post('/account', 'Account#update');

session_start();
try {
    $router->run();
}
catch (RouterException $ex) {
    $error = new ErrorController();
    $error->index();
}